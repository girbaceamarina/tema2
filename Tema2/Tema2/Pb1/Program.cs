﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pb1
{
    class Program
    {
        public static bool verifyIfLetter(string s)
        {
            var isNumeric = int.TryParse(s, out int n);
            if (s.Length==1 && isNumeric == false)
                return true;
            return false;

        }
        static void Main(string[] args)
        {
            // Problema 1
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\AL073094\source\repos\Tema2\Pb1\data.txt");
            Console.WriteLine("{0}", String.Join(", ", lines));

            //Problema 2
            Console.WriteLine("======================");

            var allCities = new List<string>() { "BUCURESTI", "CRAIOVA", "BRASOV", "CONSTANTA",
                 "CLUJ", "IASI" };

            Console.WriteLine("Letter to start with:  ");

            var startLetter = Console.ReadLine();

            while (verifyIfLetter(startLetter) == false)
            {
                Console.WriteLine("INCORRECT INPUT TRY AGAIN :Letter to start with:  ");
                startLetter = Console.ReadLine();
            }

            Console.WriteLine("Letter to end with:  ");

            var endLetter = Console.ReadLine();

            while (verifyIfLetter(endLetter) == false)
            {
                Console.WriteLine("INCORRECT INPUT TRY AGAIN :Letter to end with:  ");
                endLetter = Console.ReadLine();
            }
            
            var result = allCities.Where(x => x.StartsWith(startLetter.ToUpper()) &&
                                           x.EndsWith(endLetter.ToUpper()));
            if (result.Count() == 0)
            {
                Console.WriteLine("No cities matching the pattern");
            }

            else

            {
                foreach (string city in result)
                {
                    Console.WriteLine(city);
                }
            }



        }
    }
}
